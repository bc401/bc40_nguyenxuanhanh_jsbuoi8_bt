// Tạo mảng
var arr = [];
function taoarr() {
    const nhapsoEl = document.getElementById('txt-nhapso').value * 1;
    document.getElementById('txt-nhapso').value = "";
    arr.push(nhapsoEl);
    document.querySelector("#result").innerHTML = ` ${arr}`;
}
// Tổng số dương
function tongsoduong() {
    var count = 0;
    for(var i=0; i< arr.length; i++) {
        const num = arr[i];
        if (num > 0) {
            count += num;
        }
    }
    document.getElementById("result1").innerHTML = count;
}
// Đếm số dương
function demsoduong() {
    var count = 0;
    for(var i=0; i< arr.length; i++) {
        const num = arr[i];
        if (num >= 0) {
            count ++;
        }
    }
    document.getElementById("result2").innerHTML = count;
}

// Tìm số nhỏ nhất
function timsonhonhat() {
    var min = arr[0];
    for(var i=1; i< arr.length; i++) {
       if(min>arr[i]){
        min = arr[i];
       }   
    }
    document.getElementById("KQ-sonhonhat").innerHTML = min;
}

// Tìm số dương nhỏ nhất
function timsoduongnhonhat() {
    var arrNew = [];
    for(var i=0; i<arr.length; i++) {
        if(arr[i] > 0) {
            arrNew.push(arr[i]);
        }
    }    
    
    if(arrNew.length >0) {
        var min = arrNew[0];
        for(var i=0; i< arrNew.length; i++) {
            if(min>arrNew[i]){
                min=arrNew[i];

            }
        }
    }
    document.getElementById("KQ-soduongnhonhat").innerHTML = min;
}
// Tìm số chẵn cuối cùng trong mảng
function timsochancuoicung() {
    var arrNew = [];
    for(var i=0; i<arr.length; i++) {
        if(arr[i] % 2 == 0) {
            arrNew.push(arr[i]);
        }
    }    
        var min = arrNew[0];
        for(var i=0; i< arrNew.length; i++) {
            if(min<arrNew[i]){
                min=arrNew[i];
            } else {
                min = "-1";
            }
        
    }
    document.getElementById("KQ-sochancuoicung").innerHTML = min;
}
// Đổi vị trí trong mảng
function doivitri() {

}
// Sắp xếp mảng theo thứ tự tăng dần

function sapxep() {
    function sapxepmang(a, b) {
        return a - b;
    }
    ketqua = arr.sort(sapxepmang);
    document.getElementById("KQ-sapxep").innerHTML = ketqua;
}

// Tìm số nguyên tố đầu tiên trong mảng

function KTsonguyento() {
  for (i = 0; i < arr.length; i++) {
    kiemtraSNT = true;
    for (var j = 2; j <= Math.sqrt(arr[i]); j++) {
      if (arr[i] % j === 0) {
        kiemtraSNT = false;
      }
    }
    if (kiemtraSNT === true && arr[i] !== 1) {
      ketQua = arr[i];
      break;
    } else {
      ketQua = -1;
    }
  }
  document.getElementById("KQ-songuyento").innerHTML = ketQua;
}

